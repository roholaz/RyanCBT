from plotly.graph_objs import *
from plotly.offline import plot as offpy
import plotly.graph_objs as go
import os


def grouped_bar_chart_plot_based_on_session(data, result_dir):
    participants = list(data.keys())
    ys = [[item[1] for item in  data[participant]]  for participant in participants]

    session_names = ["session "+str(i+1) for i in range(7)]
    data = []
    for i, y in enumerate(ys):
        trace = go.Bar(
            x=session_names,
            y=y,
            name="subject "+str(i),
            textposition='auto',
            marker=dict(
                color='rgb('+str(20+20*i)+','+str(200-40*i)+',225)',
                line=dict(
                    color='rgb(8,48,107)',
                    width=1.5),
            ),
            opacity=0.6
        )
        data.append(trace)

    layout = go.Layout(
        xaxis=dict(
            title='Sessions',
            showgrid=True,
            showline=True,
            zeroline=False,
            titlefont=dict(
                family='Arial, sans-serif',
                size=18,
                color='black'
            ),
            showticklabels=True,
            # tickangle=45,
            tickfont=dict(
                # family='Old Standard TT, serif',
                size=14,
                color='black'
            ),
            exponentformat='e',
            showexponent='all',
        ),
        yaxis=dict(
            title='Scaled Sentiment Value',
            showgrid=True,
            showline=True,
            zeroline=False,
            titlefont=dict(
                family='Arial, sans-serif',
                size=18,
                color='black'
            ),
            showticklabels=True,
            # tickangle=45,
            tickfont=dict(
                # family='Old Standard TT, serif',
                size=14,
                color='black'
            ),
            exponentformat='e',
            showexponent='all',
            range=[0.0, 0.55]
        ),
    )


    fig = go.Figure(data=data, layout=layout)
    offpy(fig, filename=result_dir+"based_on_session_grouped-bar-direct-labels.html", auto_open=True, show_link=False)


def grouped_bar_chart_plot_based_on_person(data, participants, result_dir):
    num_sessions = len(data[participants[0]])
    ys = [[(data[participant][session][1], participant) for participant in participants] for session in range(num_sessions)]

    colors = ['rgb(165,0,38)',
              'rgb(215,48,39)',
              'rgb(244,109,67)',
              'rgb(253,174,97)',
              'rgb(254,224,144)',
              'rgb(224,243,248)',
              'rgb(171,217,233)',
              'rgb(116,173,209)',
              'rgb(69,117,180)',
              'rgb(49,54,149)']

    colors = ["#66B2FF",
              "#3399FF",
              "#0080FF",
              "#0066CC",
              "#3333FF",
              "#0000FF",
              "#0000CC"]

    trances = []
    for i in range(num_sessions):
        trace = go.Bar(
            x=["Subject "+str(j+1) for j in range(len(ys[i]))],
            y=[name[0] for name in ys[i]],
            name='Session '+str(i+1),
            textposition='auto',
            outsidetextfont=dict(family='Times New Romans',),
            marker=dict(
                color= colors[i],#'rgb('+str(20+20*i)+','+str(20+10*i)+', 255)',

                line=dict(
                    color='rgb(8,48,107)',
                    width=1.0),
            ),
            opacity=1.0
        )
        trances.append(trace)

    layout = go.Layout(
        xaxis=dict(
            tickfont=dict(
                size=22,
                color='#000000'
            ),

            titlefont=dict(
                family='Times New Romans',
                size=22,
                color='black'
            ),

            mirror=True,
        ),

        yaxis=dict(
            title='Word Count',
            showgrid=True,
            showline=True,
            zeroline=False,
            titlefont=dict(
                family='Times New Romans',
                size=22,
                color='black'
            ),
            showticklabels=True,
            # tickangle=45,
            tickfont=dict(
                # family='Old Standard TT, serif',
                size=23,
                color='black'
            ),
            exponentformat='e',
            showexponent='all',
            #range=[0.0, 0.55]
            mirror=True,
        ),

        legend=dict(x=0.87,
                    y=1.0,
                    font=dict(
                        family='Times New Romans',
                        size=20,
                        color='#000'
                    ),
                    )
    )


    fig = go.Figure(data=trances, layout=layout)
    offpy(fig, filename=result_dir+"based_on_person_grouped-bar-direct-labels.html", auto_open=True, show_link=False)



def sentiment_plot(sentiment_data,
                   y_positive_regressed,
                   y_negative_regressed,
                   positive_slope,
                   negative_slope,
                   participant,
                   result_dir):
    negatives = []
    positives = []
    neutrals = []
    for session_num, sentiments in sentiment_data:
        negatives.append(sentiments["negative"])
        positives.append(sentiments["positive"])
        neutrals.append(sentiments["neutral"])

    trace_positive = go.Scatter(
        x=list(range(1, len(sentiment_data)+1)),
        y=positives,
        mode='markers',
        name='Positive',
        marker = dict(
            size=10,
            color='#04B404',
            symbol= 'star',
            line=dict(
                width=0,
            )
        )
    )

    trace_negative = go.Scatter(
        x=list(range(1, len(sentiment_data)+1)),
        y=negatives,
        mode='markers',
        name='Negative',
        marker=dict(
            size=10,
            color='rgba(255, 0, 0, 1)',
            line=dict(
                width=0,
            )
        )

    )



    positive_regressed_trace = go.Scatter(
        x= list(range(1, len(sentiment_data) + 1)),
        y= list(y_positive_regressed.reshape(1,-1)[0]),
        mode='lines',
        name='Positive Regression',
        line = dict(color='#04B404', width=5),
    )

    negative_regressed_trace = go.Scatter(
        x=list(range(1, len(sentiment_data) + 1)),
        y=list(y_negative_regressed.reshape(1, -1)[0]),
        mode='lines',
        name='Negative Regression',
        line=dict(color='rgba(255, 0, 0, 1)', width=5),

    )

    data = [trace_negative, trace_positive, positive_regressed_trace, negative_regressed_trace]

    layout = go.Layout(
        xaxis=dict(
            title='<b>Sessions</b>',
            showgrid=True,
            showline=True,
            mirror=True,
            zeroline=False,
            titlefont=dict(
                family='Times New Romans',
                size=22,
                color='black'
            ),
            showticklabels=True,
            #tickangle=45,
            tickfont=dict(
                #family='Old Standard TT, serif',
                size=22,
                color='black'
            ),
            exponentformat='e',
            showexponent='all',

        ),
        yaxis=dict(
            title='<b>Scaled Sentiment Value</b>',
            showgrid=True,
            showline=True,
            mirror = True,
            zeroline=False,
            titlefont=dict(
                family='Times New Romans',
                size=22,
                color='black'
            ),
            showticklabels=True,
            #tickangle=45,
            tickfont=dict(
                #family='Old Standard TT, serif',
                size=22,
                color='black'
            ),
            exponentformat='e',
            showexponent='all',
            range=[0.0, 0.55]
        ),

        # annotations=[
        #     dict(
        #         x=2,
        #         y=0.5,
        #         xref='x',
        #         yref='y',
        #         text='<b>positive slope=' + str(round(positive_slope,3))+"</b>",
        #         showarrow=False,
        #         font=dict(
        #             family='Courier New, monospace',
        #             size=20,
        #             color='#000000'
        #         ),
        #         align='center',
        #         arrowhead=2,
        #         arrowsize=1,
        #         arrowwidth=2,
        #         arrowcolor='#636363',
        #         ax=20,
        #         ay=-30,
        #         bordercolor='#ffffff',
        #         borderwidth=2,
        #         borderpad=4,
        #         bgcolor='#ffffff',
        #         opacity=0.8
        #     ),
        #
        #     dict(
        #         x=2,
        #         y=0.46,
        #         xref='x',
        #         yref='y',
        #         text='<b>negative slope=' + str(round(negative_slope,3))+"</b>",
        #         showarrow=False,
        #         font=dict(
        #             family='Courier New, monospace',
        #             size=20,
        #             color='#000000'
        #         ),
        #         align='center',
        #         arrowhead=2,
        #         arrowsize=1,
        #         arrowwidth=2,
        #         arrowcolor='#636363',
        #         ax=20,
        #         ay=-30,
        #         bordercolor='#ffffff',
        #         borderwidth=2,
        #         borderpad=4,
        #         bgcolor='#ffffff',
        #         opacity=0.8
        #     )
        # ],

        legend=dict(x = 0.80,
                    y = 1.0,
                    font=dict(
                        family='Times New Romans',
                        size=20,
                        color='#000'
                    ),
                )

    )

    fig = go.Figure(data=data, layout=layout)
    offpy(fig, filename=result_dir+"sentiments_over_sessions_"+participant+".html", auto_open=True, show_link=False)


def plot_table(values, participant, result_dir):
    trace0 = go.Table(
        columnorder=[1, 2],
        columnwidth=[80, 400],
        header=dict(
            values=[[''],
                    ['<b>Positive</b>'],
                    ['<b>Negative</b>']],
            line=dict(color='#506784'),
            fill=dict(color='#119DFF'),
            align=['left', 'center'],
            font=dict(color='white', size=12),
            height=40
        ),
        cells=dict(
            values=values,
            line=dict(color='#506784'),
            fill=dict(color=['#25FEFD', 'white']),
            align=['left', 'center'],
            font=dict(color='#506784', size=12),
            height=30
        ))

    data = [trace0]
    fig = dict(data=data)
    offpy(fig, filename=result_dir+"table_"+participant+".html", auto_open=True, show_link=False)



def participants_sentiment_plot(participants_sentiments, result_dir):
    negatives = {}
    positives = {}
    neutrals = {}

    for participant in participants_sentiments:
        negatives[participant] = []
        positives[participant] = []
        neutrals[participant] = []
        for session_num, sentiments in participants_sentiments[participant]:
            negatives[participant].append(sentiments["negative"])
            positives[participant].append(sentiments["positive"])
            neutrals[participant].append(sentiments["neutral"])


    positive_traces = []
    negative_traces = []
    neutral_traces = []

    for i, participant in enumerate(participants_sentiments):

        positive_trace = go.Scatter(
            x=list(range(7)),
            y=positives[participant],
            mode='lines+markers',
            name='positive',
            xaxis='x'+str(i+1),
            yaxis='y'+str(i+1)
        )

        negative_trace = go.Scatter(
            x=list(range(7)),
            y=negatives[participant],
            mode='lines+markers',
            name='negative',
            xaxis='x' + str(i+1),
            yaxis='y' + str(i+1)

        )

        neutral_trace = go.Scatter(
            x=list(range(7)),
            y=neutrals[participant],
            mode='lines+markers',
            name='neutral',
            xaxis = 'x' + str(i+1),
            yaxis = 'y' + str(i+1)
        )

        positive_traces.append(positive_trace)
        negative_traces.append(negative_trace)
        neutral_traces.append(neutral_trace)

    data = positive_traces + negative_traces + neutral_traces

    layout = go.Layout(
        xaxis=dict(
            domain=[0, 0.45]
        ),
        yaxis=dict(
            domain=[0, 0.45]
        ),
        xaxis2=dict(
            domain=[0.55, 1]
        ),
        xaxis3=dict(
            domain=[0, 0.45],
            anchor='y3'
        ),
        xaxis4=dict(
            domain=[0.55, 1],
            anchor='y4'
        ),
        yaxis2=dict(
            domain=[0, 0.45],
            anchor='x2'
        ),
        yaxis3=dict(
            domain=[0.55, 1]
        ),
        yaxis4=dict(
            domain=[0.55, 1],
            anchor='x4'
        )
    )

    fig = go.Figure(data=data, layout=layout)

    offpy(fig, filename="nnnn.html", auto_open=True,
          show_link=False)

def example():
    trace1 = go.Scatter(
        x=[1, 2, 3],
        y=[4, 5, 6],
        xaxis='x1',
        yaxis='y1'
    )
    trace2 = go.Scatter(
        x=[1, 2, 3],
        y=[6, 2, 7],
        xaxis='x2',
        yaxis='y2'
    )

    trace22 = go.Scatter(
        x=[1, 2, 3],
        y=[3, 4, 9],
        xaxis='x2',
        yaxis='y2'
    )

    trace3 = go.Scatter(
        x=[1, 2, 3],
        y=[5, 7, 8],
        xaxis='x3',
        yaxis='y3'
    )
    trace4 = go.Scatter(
        x=[1, 2, 3],
        y=[2, 3, 2],
        xaxis='x4',
        yaxis='y4'
    )
    data = [trace1, trace2, trace22,  trace3, trace4]
    layout = go.Layout(
        xaxis=dict(
            domain=[0, 0.45],
            anchor = 'x1'
        ),
        yaxis=dict(
            domain=[0, 0.45],
            anchor = 'y1'
        ),
        xaxis2=dict(
            domain=[0.55, 1]
        ),
        xaxis3=dict(
            domain=[0, 0.45],
            anchor='y3'
        ),
        xaxis4=dict(
            domain=[0.55, 1],
            anchor='y4'
        ),
        yaxis2=dict(
            domain=[0, 0.45],
            anchor='x2'
        ),
        yaxis3=dict(
            domain=[0.55, 1]
        ),
        yaxis4=dict(
            domain=[0.55, 1],
            anchor='x4'
        )
    )
    fig = go.Figure(data=data, layout=layout)

    offpy(fig, filename="example.html", auto_open=True,
          show_link=False)


def bar_chart_plot(x,y, output_file):
    data = [go.Bar(
        x=x,
        y=y
    )]

    fig = go.Figure(data=data)

    offpy(fig, filename=output_file+".html", auto_open=True, show_link=False)



def visualize_associations(X, Y, Z, output_file):
    trace = go.Heatmap(z=Z,
                       x=X,
                       y=Y)
    data = [trace]
    fig = go.Figure(data=data)
    offpy(fig, filename=output_file+".html", auto_open=True, show_link=False)


def show_image(image_url):
    import webbrowser
    new = 2
    html = "<html><head></head><body><img src="+image_url+" height='750' width='1500'></body></html>"
    dir_path = os.path.dirname(os.path.realpath(__file__))
    with open(dir_path+"/show_image.html", 'w') as file_writer:
        file_writer.write(html)

    dir_path = os.path.dirname(os.path.realpath(__file__))
    out_file_url = "file://"+dir_path+"/show_image.html"
    webbrowser.open(out_file_url, new=new)

if __name__ == "__main__":
    example()

