from pycorenlp import StanfordCoreNLP

from analysis.corenlp.server import StanfordCoreNLPServer


class Client():

    def __init__(self, configuration):
        '''
        As you make an instance of the client the server starts to run
        '''
        corenlp_server = StanfordCoreNLPServer(corenlp_dir=configuration['jar_dir'], port=configuration['port'])
        corenlp_server.run()

        self._url = str(configuration['ip']) + ":" + str(configuration['port'])

    def get_sentence_sentiment(self, sentence):
        try:

            stanford_corenlp = StanfordCoreNLP(self._url)
            result = stanford_corenlp.annotate(sentence,
                                               properties={
                                   'annotators': 'sentiment',
                                   'outputFormat': 'json',
                                   'timeout': 5000,
                               })
        except Exception as ex:
            print(ex)
            return None

        try:
            sentiment, sentiment_value = result["sentences"][0]["sentiment"].lower(), result["sentences"][0]["sentimentValue"].lower()

        except:
            print(sentence)

        return sentiment, sentiment_value


    def get_sentences_sentiments(self, sentences):
        try:

            stanford_corenlp = StanfordCoreNLP(self._url)
            result = stanford_corenlp.annotate(sentences,
                                               properties={
                                   'annotators': 'sentiment',
                                   'outputFormat': 'json',
                                   'timeout': 5000,
                               })
        except:
            return None

        sentiments = [sentiment_info["sentiment"] for sentiment_info in result["sentences"]]
        sentiment_values = [sentiment_info["sentimentValue"] for sentiment_info in result["sentences"]]
        return sentiments, sentiment_values

if __name__ == "__main__":
    config = {}
    config['ip']= "http://localhost"
    config['port'] = "9000"
    config['jar_dir'] = "/home/rohola/Codes/Ryan/RyanCBT/analysis/libs/stanford-corenlp-full-2018-02-27/*"
    c = Client(config)
    print(c.get_sentence_sentiment("I am happy."))
