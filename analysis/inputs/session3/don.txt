R: Hi Don. Welcome to Session 3. How are you doing today? 

 
P: I’m doing good. 

 
R: That's good. Great. Let's continue. I hope you are still using your daily route scale. By now you have most likely finished an entire sheet. So I'm going to have my friend give you another four this coming week. When you are ready to continue say ready.  

 

[student gives sheet] 

 

P: Ready 

 

R: Great. I want you to try and pay attention to the patterns in your move day to day. It might teach you something. Also you should have been keeping track of the number of positive and negative thoughts throughout the day. This is important because we want to encourage healthy thinking a good way to do this is to focus on thoughts that produce a better mood. You could increase the number of good thoughts in your mind such as making a list of good things about new in your life. You should also not be afraid to pat yourself on the back sometimes. A lot of what we do isn't noticed by others so you should give yourself credit for what you do. Another good thing to do is take breaks around your mind to relax so your body can feel at peace that peace can give you energy. Finally consider the future. Imagine yourself taking steps toward a time when things are better. Let's try and exercise. Tell me some positive things about yourself.  

 

P: I don’t know. Let’s see. Sometimes I can’t think. 

 
R:  that's okay. I think you're very kind. That's positive. Great job. I love those ideas. You are an amazing person and you shouldn't forget that. What are some positive things about your life? 

 
P: I don’t know. I just have a lot of friends to deal with in my life. It’s a lot different than it used to be.  

 

R: That's good friends are positive. You also have an amazing daughter. 

 

P: Thank you. She’s very good. 

 

R: You’re welcome. Those things really are great. Now think about how you feel after talking positively about yourself and your life. Do you feel different? 

 

P: No worse maybe a little better. 

 

R: I see. I think wrongly it feels much better thinking this way rather than negatively. We actually want to try and decrease those thoughts that make us feel bad if in fact it's ruining your mood. You should strive to identify this and tell yourself to think of something that won't disrupt your well-being. What are some things you can do to accomplish this? 

 

P: Sometimes I read something to feel better. I guess I feel better when I talk to people and visit with them.  

 

R: exactly. Talking with others improves our mood I see. Let's continue I want to discuss an alternate approach to work through your thoughts that I feel can be really rewarding. You can think of it like talking back to the thought that's hurting you. This method is called the ABC method A stands for the activating event is the belief you have C stands for a consequence. Family Day is the way you describe the thought. Another way to look at it is consider what happened. What do you tell yourself about what happened. How do you feel as a result and how you talk back to the fact I've prepared a print out for you to read and use to practice this method. Do you have any questions? 

 

P: No I don’t. 

 

[student hands printout] 

 

R: good this may seem silly or confusing but once we walk through some examples it will make more sense to practice. Pretend I'm feeling down because I was diagnosed with diabetes. In this situation this is not a part of the method because it is a fact and something causing my sadness. These two choices. Which do you think applies to the belief part of the method? 

 

P: I don’t know. I had the diabetes for forty years I guess and stuff. So it's been under control. I don't worry too much about it now. 

 

R: You did. That's great. You keep it under control. You really healthy? 

 
P: Excuse me? 

 

R: Doing a healthy which is  

 

P: I would say yes. 

 

R: that's good. You can answer my question by taping on the tablet. Which do you think is a belief? 

 

P: My diabetes. It won’t ruin my life I guess. Especially now it’s getting too short [laughs] 

 

[student explains how to use tablet, tablet freezes, pause as students try and fix tablet] 

 

R: Good job Don. This answer is a belief I hold about my diabetes and not an actual fact let's try the next part. Thank you. Which statement is a consequence of my heart. Which statement is a consequence of my thought? 

 

P: [picks answer] 

 

R: Nice try but I think this answer is more of a belief than a consequence. Me Feeling sad is a direct consequence of my negative thoughts about diabetes. Let's finish this exercise by looking at the final letter of the ABC method. Do you have any ideas on how I could dispute my negative thoughts about having diabetes? 

 

P: I guess it ain’t so bad if you take care of it cuz I’ve had it for a long time and I’m doing alright. I have to watch what I eat a little bit. 

 

R: Exactly clear when your health is great so it can't be that bad. Let's continue. Here's one I thought of. I now have diabetes but can still live a satisfying life. In fact this statement is equal parts true as it is optimistic. I hope this exercise made it easier to understand the method you've been doing great. Believe it or not we've already been through four sessions. This wraps up our section about how thoughts affect your mood. To summarize how important thoughts are. I'm going to show you a video about the science behind them. 

 

[video plays] 

 

R: That was great. Did you find it interesting or learn anything interesting? 

 

P: Yes it’s interesting. I don’t know if I found anything different.  

 

R: If his thoughts are so important do you know a lot about this already? 

 

P: Yes. 

 

R: Wow that's great. I'm glad you've got something meaningful out of it. As we've talked about our thoughts are incredibly important to our health and well-being. Next time we are going to begin looking at how activities affect our mood into our next session try and practice the ABC method and work on your daily mood scale. Great job today. Goodbye.  

 
