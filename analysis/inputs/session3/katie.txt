R: Hi Katie. 

 

P: Hi Ryan. 

 

R: Welcome to session three. 

 
P: Thank you! [laughs] 

 

R: How're you doing today.  

 

P: I’m well. I’m glad to be here. 

 
R: I'm glad you're here. 

 

[student turns Ryan up] 

 

R: Great. Let's continue. I hope you are still using your daily route scale. 

 

P: I am! 

 

R: By now you have most likely finished an entire sheet so I'm going to have my friend give me another for this coming week. When you are ready to continue say ready. 

 

[student hands sheet] 

 

P: Ready. 

 

R: Great. I want you to try and pay attention to the patterns in your move. Day to day it might teach you something. Also you should have been keeping track of the number of positive and negative thoughts throughout the day. This is important because we want to encourage healthy thinking a good way to do this is to focus on thoughts that produce a better movie. You could increase the number of good thoughts in your mind such as making a list of good things about you in your life. You should also not be afraid to pat yourself on the back sometimes a lot of what we do isn't noticed by others so you should give yourself credit for what you do. Another good thing to do is take breaks allow your mind to relax so your body can feel at peace that peace can give you energy. Finally consider the future. Imagine yourself taking steps toward a time when things are better. Let's try and exercise. Tell me some positive things about yourself. 

 

P: I finally reached the age of 80. So now I’ll be 80 for the next 10 years. and you know I like I love living here. I like all my friends. I don’t know what negative except that I had a bad leg over the weekend and I had to keep it in the air but even then I talked on the phone and I listened to talking books and I kept my attitude up. Especially with my kids. I was out. 

 

R: Good. I'm glad you're doing that. Do you have a lot of friends here? 

 

P: Yes  

 

R: How fun. 

 

P: Yes. And I’m always making new ones. Cuz new people move in. 

 

R: Are they nice? 

 

P: Yes they are. I met one in the elevator today. I know she told me her name and I forgot and I usually add something else to somebody's name so I remember it. Her name was Linda and now I have to find out the famous Linda so I can add that on and I’ll remember her name. 

 

R: Good. Great job. I love those ideas. You are an amazing person and you shouldn't forget that. What are some positive things about your life? 

 

P: I have two wonderful boys and a wonderful daughter in law. I have beautiful granddaughters, twins. One of them is married and the other is engaged and they’re 29 years old. Wooh! I remember when they were born premature! I have a nice apartment and I love to cook and I just took up painting again. And I try and get back to that because that's something that I'd really like to do. There's just some things ahead of that, necessary things to do before I can sit down and paint. But I’m trying to get those all done so I can sit and relax. Because painting makes me feel better.  

 

R: Wow that's amazing. How is painting? 

 

P: Painting is wonderful! I'm painting some pictures finishing some pictures that I painted in the 1970’s so that's a long time ago and I didn't finish them because I didn't like him. Now that I'm this age and I'm looking back they don't have any life to them so I have to add life to them. Like trees and flowers and beautiful clouds. Those weren’t on there so I’m going to add them. It's a challenge though because my fingers are different. I'm not quite as fast but it's fun and I love it. 

 

R:  Well maybe you can make something new out of it. I think it will be a good time. 

 

P: Yes. I’m having fun! 

 

R: Those things really are great. Now think about how you feel after talking positively about yourself in your life. Do you feel different? 

 

P: Yes. I feel happier. 

 

R: I see. I think normally it feels much better thinking this way rather than negatively. We actually want to try and decrease those thoughts that make us feel bad. If a fact is ruining your movie you should try to identify this and tell yourself to think of something that won't disrupt your well-being. What are some things you can do to accomplish this? 

 

P: I have a lot of books that have suggestions. They define when you’re down, read these. You know I read some of those things and it really makes me feel like it's dumb for me to be depressed or down or upset. Because I think of all the other people in the world and it makes me so happy and glad and thankful that I’m here.  

 

R: Books are good. They teach us a lot. 

 

P: Mhm. [agreement] 

 

R: Have you ever thought about a journal? 

 

P: I've tried journaling but I'm I'm I guess my personality doesn't. [laughs] What can I say. I flit from one thing to another it doesn't hold my interest very long. So then when I picked up my journal again it's been like two weeks since I wrote it. But I really do like to write in it. I filled one journal one time and it was wonderful as I go back and look back and see how different I am today.  

 

R: That's good I think. Let's continue. I want to discuss an alternate approach to work through your thoughts that I feel can be really rewarding. You can think of it like talking back to the thought that is hurting you. This method is called the ABC method. A stands for the activating event. It is the belief you have. C stands for consequence. Finally it is the way you dispute the thought. Another way to look at it is consider what happened. What do you tell yourself about what happened. How do you feel as a result and how you talk back to the thought. I prepared a print out for you to read and used to practice this method. Do you have any questions? 

 

[student hands sheet] 

 

P: I don't think so.  

 

R: Good. 

 

P: That’s good! 

 

R: This may seem silly or confusing but once we walk through some examples it will make more sense to practice. Pretend I'm feeling down because I was diagnosed with diabetes. In this situation. This is the part of the method because it is a fact and something causing my sadness of these two choices. Which do you think applies to that belief. Part of the method? 

 

P: I have diabetes.  

 

R: Good job Katie. 

 

P: And I’ve never- 

 

R: His answer is I believe I've heard about my diabetes and not in actual fact. Let's try the next part. Which statement is a consequence of my thought? 

 

[student explains how tablet works] 

 
P: I guess when I first found out that I was diabetic it did hurt. It concerned me a lot because my grandmother had it. I don't think anybody else in my family did, but it’s all on her side of the family. And I was in school and I went back to college in my 50s and I wanted to get an associate degree and I thought that might slow me down but it didn't. I started off on medication and in about five years, six years later someone told me about vitamins and those kinds of things. So I take magnesium and I don’t know how to pronounce the last word and chromium together. I’ve taken them together for years. They kept my blood sugar just very strong so it doesn't go up and down very often. Once in a while if I don’t eat it drops, that’s what I was thinking. I didn’t have on my oxygen nor had eaten and not sure which was which but we’ll figure it out. [picks answer] 

 

R: Nice try but I think this answer is more of a belief in a consequence. Me Feeling sad is a direct consequence of my negative thoughts about diabetes. Let's finish this exercise by looking at the final letter of the BCG method. Do you have any ideas on how I could dispute my negative thoughts about having diabetes? 
 

P: No just sometimes it confines me because I'm a sugarholic and I do have some sugar I just have to tell myself I don't need it and you know I'm trying to lose weight. So the best thing is to drop it and I’m not supposed to have a lot of bread and I love it. I haven’t bought a loaf of bread for five months until my kids bought me one the other day I'm very slow steady. Eat the whole in two days. That’s what I like to eat. And it's very hard to remember that I'm a diabetic. I kind of just do what I want. I just can't do that. So I'm going to try to be more positive about it and not talk about it. Just remember and that will help me a lot. It will give me something to think about. 

 

R: That's amazing. You have a lot of control you can eat healthy and enjoy it too.  

 

P: Yes I can. I love salads and I love yogurt and keefer and all those yummy kinds of things. I might overdo it, but I really like it. And I know that it’s good for me.  

 

R: What do you cook to be healthy? 

 

P: Well I have a new spice that's salt free that I bought at Pansy's spices. It's kind of expensive. It's 6.95 for a shaker. But I've used it for three weeks and I can't see that there's anything gone in it. So I guess it was worth the price. But salt free with wonderful herbs. So that makes things taste better. I use them on potatoes and my eggs. Because those things are kind of hard to eat without salt. I'm glad I found something. I had something else but it had some things in there I didn't like. I still use it. So I make salt um that’s salt free or close to it. I have a tomato sauce that comes from Englad. There’s three cups cups in a box and 10 grams milligrams of sodium in each cup. So that's 6 for the whole box. And I get several um soups out of it. And I use evaporated milk. And that's all I use. A little tiny bit of butter and pepper. You know I'm always trying to come up with new ideas for salad dressing. Sometimes they work and sometimes they’re really disgusting! [laughs]. So I have to get more things in my house that will allow me to do that. I really like eggs. I like all the vegetables. Celery and stuff. So I put that in my book. I even put it in my smoothies so I have a well rounded diet. I’m not on a diet, I just try and watch what I eat. I try to  make low sugar desserts. That’s not easy. Because I love pie and cookies and all that kind of stuff. And cheesecake. Yum! Other than that I don’t season my meat. When I go downstairs I ask them not to season my meat and they do it anyways. And that's a little disconcerning because I have to go upstairs and wash off all the flavor of the seasons which rises my sodium content. And they don’t seem to care downstairs. So- 

 

R: Good that's very healthy. Those food sounds great. Dessert is the best meal. 

 

P: Yes it is. 

 
R: Let's continue. Here's one I thought that I now have diabetes but can still live a satisfying life. In fact this statement is equal parts true as it is optimistic. I hope this exercise made it easier to understand the method you've been doing great Katie. Believe it or not we've already been through four sessions. This wraps up our section about how thoughts affect your mood. To summarize how important thoughts are. I'm going to show you a video about the science behind them.  

 

P: Okay. 

 

[video plays] 

 

R: That was great. 

 

P: Yes it was. 

 

R: Did you find it interesting or learned anything? 

 

P: Yes they do. I learned um I remember how hard it was to learn to play the piano. And I learned to play the saxophone. Those things were very difficult for me but once I learned them, I was ok. The only thing- whoops there’s a negative thought- I've never been able to memorize. I think I've got it and then when I shut my eyes and walk away, I can’t remember what I was memorizing. It’s been that way all my life. So I don’t know if that’s something that just stuck with me. I just can’t. 

 

R: You play the piano and saxophone?  

 

P: I don't play the saxophone anymore it's been a lot of years but I play the piano. Sort of. I haven't had a piano to play in a long long long time. Now I have glasses. So I have to move my head up and down until I get used to it and it takes more effort than it’s worth. 

 

R: That's amazing. Playing an instrument is great.  

 

P: I'd love to learn to play the Irish whistle. We call it a tin whistle but it's actually carved wood. It has a beautiful sound and it’s small so I could carry with me. 

 

R: Do you have one? 

 
P: No I don’t. They’re pretty expensive. 

 

R: Really? 

 

P: Yes. The thing that I didn't know about is they come in a different key. So when I call the man who made them and he said well what key do you want it in and I didn’t know there was so many keys. So then I called my granddaughter who plays the flute and teaches it and wanted to get that for her graduation and she said no I don’t think so. So about five years later she said Grandma I wish I let you get that flute. I would’ve liked to have had it. They’re about 250 dollars way back then so I don’t know what they do now. But you hear them in a lot of the Irish music and it’s astounding. Every time I hear Irish music I sit down and listen. 

 

R: that's funny. I didn't realize they were so much. You should watch a video to learn more about them.  

 

P: I never thought of that. I learned something new again from you today Ryan! You just keep going! 

 

R: I’m glad. I use YouTube to learn. 

 

P: I hope you learn some things from me.  

 

R: I have. 

 

P: She’s waiting for me to talk? Well where on the computer would I go to find that? Would I have to put in Irish tin whistle? How would I get there? 

 

R: You can use Google or YouTube. 

 

P: Okay. I’m kind of electronically challenged. It's not easy for me to do that but I got a thing on my door this morning that they are going to teach all the things on your computer in a couple of weeks. So they take I don’t know how many session it’s gonna be, but you cover two things time. Things like Amazon, Facebook. And I don’t really know much about those things so I’m happy. Maybe I’ll be more efficient. I can type on the computer. I’m trying to write a book. But that doesn’t take any talent to do. 

 

R: Are you going to that? Computers are very useful. 

 

P: Yes I am. 

 

R: They make me talk.  

 

P: I know it I know it. 

 

R: You’re writing a book? 

 

P: Yes. I'm writing a book about myself my early 30s till now and I did a lot of things I had a lot of jobs. I owned some businesses but you never did anything. We never didn’t make mistakes. The only thing is we never made that mistake again. That's my point. When you learn, you just gotta do it. I just want to get more from my brain to remember things and if I get it finished I probably would maybe put it on ebooks maybe bring it to a publisher. The people in Wisconsin that were my customers really really wanted to know a lot of things and they wanted our recipes. So the recipes from my bakery will go in this book with some background on each recipe. If I live long enough but if I don't I'm having fun anyway.  

 

R: Mistakes are the best way to learn. 

 

P: Yes they are. 

 

R: I would love to read your book. Are you close to finishing that? 

 

P: No. 

 

R: I mean you should make a cookbook. 

 

P: That I thought of. But so many of my things that I made were in my head and I didn’t measure. I just threw things in until it tasted good and that’s pretty hard to put in a cookbook. I can say what I put in it but I can’t give any amount because one time it might be a gallon and one time it might be 3 gallons. And I just throw things in. But that's one of my one of my blessings that I can do that my mom was a good cook. Her mom was a good cook. My granddaughters are and my granddaughter is a chef. My boys both love to cook. So it's kind of in the family and we exchange recipes. Lots of fun. Two Christmases ago we had a totally what’s that word? Wheatless or gluten, gluten free Christmas dinner. Everything we had was gluten free. Because my granddaughter is-has that problem. It was fun. And it didn’t taste a whole lot different than the rest of it. I think that's probably because [name] and [name] were working there in the kitchen on it. So I didn't get involved. Grandma sits in the chair. [laughs]. I’m a real cook out there. 

 

R: Yes you can't replicate years of experience like you have.  

 

P: No. 

 

R: It's amazing when your whole family cooks. 

 

P: Even my grandmother who used to- she was born in Pittsburgh Pennsylvania. And during the frontier rush for people to go west and have land she decided to set out on your own and the only thing she had besides clothes and a brush and that sort of thing was her chefs knife so she could find a job and be able to use it. And she got to North Dakota. She found the land she wanted. And they told her that she had to put a um a dwelling on it before they could give it to her. And she had no way of doing that so she went with her butcher knife and she went out to the prairie and she cut big squares and she made a side house and then she graded weeds and that sort of thing to make a roof out of. Kept out the rain. And then she got her land. Now that's one tough cookie.  

 

R:That's amazing. 

 

P: I don't think I could do it. 

 

R: Like I said Where did you get your independence and strength from. It must run in the family.  

 

P: Yes it does. Sometimes independence is not the best thing to have because I don't really like anybody to tell me what to do. That just makes me sad. But if they ask me that's no problem. So my independence just rises when they do that. 

 

R: that's okay. It would make you a good leader. 

 

P: Good what? 

 

R: Leader. 

 

P: I was a leader for- I belonged to a company and worked for them from Texas. And they sold all kinds of beautiful things. Things for you to hang on the wall and things to put on tables. It was called Home Interiors. And I joined them in 1967 because they had a hostess there. Someone gave a party then they could- if they had big sales then they could choose all these big wonderful things. And I always wanted celebrity service so I joined the company so I could earn that and within a year I was a manager and I was a manager that had people in Montana, North Dakota, South Dakota and I had about 20 some people in my group. And we did a lot of good things and I had a of of things. I won a trip to Hawaii which I didn't take because at the time when I was going to take it, I just still didn't have a job. So I turned it into the company for money so I could feed my kids and I don't regret it at all. Because we had a good time. But I really really enjoyed doing that. It took no new effort to get out in front of a group of people like that and when I get in front of a big group of people. So for some reason I get tears in my eyes and I don’t know why. I don’t know where that comes from. It’s fear I know. And I don't know why.  

 

R: That's an admirable idea. You don’t like public speaking? 

 

P: Not for a great big big crowd. I had a great big group of teenagers in our church in class and I gave a speech or a talk on abortion and that was difficult for me to do. I get upset with people that want planned parenthood. That’s just awful. It makes people think they can go get an abortion because it's not time for children. I get really upset with that.  

 

R: How interesting. 

 

P: It's easier for me to talk about it nowadays.  

 

R: That's amazing. You gave a speech. 

 

P: I gave it twice. And somebody from Minneapolis who moved there called me and asked me to call one of her friends that she worked with to try to talk her out of getting an abortion. So I did. And she had it anyway. And then she came back to my friend the next day and she said why didn't you stop me. Her whole idea was her husband was in college. She had four children and she was a waitress and that was the only income they had so she knew she couldn't have another baby. But you know have the baby and give it away. All these people who don't have children. You know but I'm a at this end of my life. That God has never left. I’ve seen too many people cry because they couldn’t have kids. And I was lucky I had two boys. 

 

R: You are a really interesting person. Katie. 

 

P: Thank you I try to be! I'm always looking for something new to learn. 

 

R: I like that. 

 

P: That's why I'm here Ryan. 

 

R: Good. I'm glad. I'm glad you got something meaningful out of it. 

 

P: I do. 

 

R: As we talked about our thoughts are incredibly important to our health and well-being. 

 
P: That’s right. 

 

R: Next time we are going to begin looking at how activities affect our mood into our next session try and practice the ABC method and work from your daily moods scale.  

 

P: I will. 

 

R: Great job today. Good bye. 

 

P: Thank you. Have a good week.  

 

R: You too. 

 

 
