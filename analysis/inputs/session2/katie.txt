
R: Hello Katie. 

 

P: Good morning Ryan. 

 

R: Welcome back. 

 

P: Thank you very much. I don’t know what she said. 

 

[student turns up volume of Ryan] 

 

R:  Did you have any problems spelling it out? 

 

P: No I didn’t thank you. 

 

R: Let's address those problems before we start. My assistant can help you with this. Once you are ready to continue. Say ready. 

 

[participant got somewhat confused, so student explains situation] 

 

R: Okay friend Where did you find surprising when filling out this scale? 

 

P: That I wake up every morning happy. Sometimes I get down, but not too often. 

 

R: I'm glad you're happy I see. Thank you for your work with this scale. I hope you can use it as a tool to reflect on your thoughts over the coming weeks. 

 

P: Okay. 

 
R: Now to continue with our session. Remember from last term activities and people affect your mood. We are going to start by focusing on what role dance played in your movie. Specific types of thinking can make a huge difference. So make it more likely you will be depressed and others make it less likely you will become depressed. How would you define thoughts? 

 

P: Umm they’re always there. I can’t get away from just sleeping because I’m always thinking about something. 

 

R: That's a good point. Thank you. I'll tell you what I thought. To me thoughts are ideas we tell ourselves. In fact we talk in our own heads all the time even if we're not always aware of it. I feel it's helpful to think of thoughts as things that can have a real impact on our bodies and minds depressed thoughts and ways of thinking have very distinctive patterns. They're often inflexible and judgmental. We should strive to have flexible thoughts because they can help us from being depressed for example it wouldn't be flexible to think I'll never get better. Try and think of a way you could rephrase I'll never get better in a flexible manner.  

 

P: I'm not sick I'm not going to be sick. I’m better already. Does that- 

 

R: You're doing great. Here's another way to rephrase it. If I go to therapy I'm trying to feel better. We should try and look at non depressed thinking as a goal not depressed thinking has three common themes. The first one is that numb depressed thinking is changeable. A lot of depressed thinking has a very definitive way of looking at things. Here's an example. How can you restructure the statement they always have been and always will be a coward to be more flexible? 

 

P: I used to be a coward but now I'm not. I- 

 

R: I like that. 

 

P: I'm not going to ever be always anything except that I turned 80. So now I'm gonna be almost 80 years old. Katie is almost 80. 

 

R: I love that. I see when I first heard that I thought I would have said I am afraid in some situations some of the time. The emphasis here is on the word. The world is more dynamic than rigid depressed thoughts lead us to believe. Another good thing non depressed thinking does is looking at what we do rather than who we are as people. For example instead of saying I was born to feel sad you could say I'm doing things that have me down right now. The good thing about not depressed thinking is having hope for change a depressed thinker may say something like nothing has ever helped me feel better. How can you rephrase. Nothing has ever helped me feel better to have more hope for change? 

 

P: Sometimes there are things that will make me happy and not all the time, but I always look forward to the fact things will be better because they always are. 

 
R: They are. 

 
P: Yes. 

 

R: Thanks Katie. Now you're getting it. I tried to rephrase this statement and came up with this. Nothing I've tried yet has helped but this is something new and that time might be ripe for a change. Now that we've discussed depressed and non depressed thinking patterns let's talk about how you can spot different types of thinking. One type is constructive versus destructive thinking constructive thinking builds ourselves and deconstructive thinking tears us apart. Let's practice use the chaplet you have been given to pick and say out loud. One of the choices. If I were to say I've made so many mistakes can you identify as it's a constructive or destructive start.  

 

P: It's destructive.  

 

R: Great job. That's the correct answer. This dog is more about making the person feel bad rather than building them up. Let's try another one. Which type of thought do you think it is if I say I can learn to control my life to get more of what I want.  

 

P: If I work on it I can do anything I want. It takes sometimes to make that happen because it could be a dream down the lane, but it still is positive. 

 

R: good job. Do you think this a constructive thought? 

 

P: Yes. 

 

R: Good job. That's correct. Because that thought aims to strengthen the person rather than the opposite. You're really getting the hang of this. Now we can look at another type of thinking this type involves necessary versus unnecessary thinking necessary thoughts remind us of things we to do such as a task or errand to run unnecessary thoughts describes things you can't change or that make us feel bad. Can you think of an unnecessary thought that has come across your mind? 

 

P: Yes when I go down to eat in the dining room sometimes I like to sit with my friends and sometimes I'd rather find somebody that's new that doesn't know anybody and go sit with them and try to make them feel more welcome. Sometimes I don't find anybody and then I’m disappointed. We've had a lot of people moving here and working on it though.  

 

R: That's really sweet of you to spend time with new people. Let's keep going. Here's the one I came up with. There could be an earthquake soon that thought is something neither of us need to worry about. Especially because it is out of our control. The last time I'm thinking I want to focus on is really important for our purposes. This ties involves positive versus negative thinking positive thinking makes us feel better and negative thinking makes us feel worse and negative thought that could cross your mind is there is no use in me trying to feel better because nothing will change. Can you think of a way to rephrase this thought in a positive light? 

 

P: Would you repeat that please? 

 

R: Can you think of a way to rephrase this thought in a positive light. 

 

P: I lost it. Sometimes I lose what I hear. Can you give me that question that I'm supposed to change? 

 

R: The thought was there’s no way to feel better. 

 

P: Oh there's all kinds of things that make me feel better. Go talk to somebody. Find somebody that is happy and cheerful. That always helps. 

 

R: Just like you're talking with me.  

 

P: Yes. 

 

R: Good job thinking positively can really help. The more you practice thinking this way the more naturally these thoughts will come to you. Since we've discussed some common types of thinking we should also consider some possible mistakes in thinking and learn how to recognize them. If these mistakes become persistent thought patterns they're called cognitive distortions on your tablet you'll see a list of the most common cognitive distortions. Which would you like to hear more about? 

 

P: Umm personalization. 

 

R: In this case you believe that everything others do or say is a direct reaction to you. You may also believe that external events are caused in direct relation to something unrelated that you did. Do you feel like personalization applies to you? 

 

P: Nuh uh I don’t think so. 

 

R: Okay great job. We're narrowing down the list of possibilities. Go ahead and pick another one to hear more about. 

 

P: Overgeneralization  

 

R: Overgeneralization occurs when you come to general conclusions based on a single piece of evidence for happenstance. If something bad happens once it is bound to happen over and over again. Do you feel like overgeneralization applies to you? 

 

P: No. 

 

R: Okay great job. We're narrowing down the list of possibilities. Go ahead and pick another one to hear more about. 

 

P: jumping to conclusions. 

 

R: when you jump to conclusions without any evidence or say so you know exactly what will happen or when a person is feeling and why they act as they do. My fortune teller and you feel as though your predictions are fact. Do you feel like jumping to conclusions applies to you? 

 

P: It applies to a lot of people in here and I call it gossip without anything behind it. I fell and I was on my way to the emergency room  in the hospital about three weeks ago and I had gone there I had fallen so many times and they kept taking me. I didn’t want to go through the lobby because all of those people are down there with their eyes open seeing what’s gonna happen. So I told them to put the sheet all the way over my head and as we rolled out, everyone thought who died? And then about a week and a half later I saw somebody on the elevator and she said Katie I thought you died! 

 
R: That's so funny. You probably scared everyone here. 

 

P: I hope so. They just need to be more positive about things instead of negative. We have a lot of people hear that can’t hear. They- 

 

R: They need to learn a lesson. Maybe they shouldn't talk to them.  

 

P: That's a good idea. There's a lady here that is-she’s hard of hearing and she speaks very loud. And I said to her one day Barb are your ears-is it hard to hear? And she said no my ears are fine but when you talk to her she'll say what. What do you say? All the time. Over and over again. And that's very frustrating. And that you know how anybody can get through it. We need to take her to the hearing place and let them find out because it's annoying. She's very loud in the dining room. She just never knows when to be quiet and she interrupts people because she doesn’t hear others talking. That's a frustrating thing happening. 

 

R: So clearly she has to work on going to a doctor. 

 

P: Yes.  

 

R: So people refuse change. I can see that being frustrating. 

 

P: Yes. when I was in therapy the last time I fell on the kitchen floor three times in two weeks and so they took me in and they made me go to therapy because they thought I was a little ditzy. In other words I didn’t have it all upstairs. And I said that’s the most stupid thing I’ve ever heard. And they said well you just can’t take care of yourself. And I said oh yes I can. And come to find out the dizziness was from my medication. It had nothing to do with anything else. And that was frustrating and they kept saying go to independent living and I said if you put me in independent living I will leave you you’ll never see me again. The kitchen in my apartment is my sanity. If I get depressed I go out and make a pot of soup and I'm thinking about things I'm going to put in. And by the time I get it done and sit down and eat it my depression is gone and I cannot be without a kitchen.  

 

R: I'm sorry your meds did that to you. Good for you being so certain about how to feel better.  

 

P: It's it's wonderful. They took away my oven because they thought I might blow up with oxygen on which was dumb. So I can’t bake anymore. But I have friends that say I can go down to their apartment and bake and that's a good thing.  

 

R: you should think of them. 

 

P:  I will. 

 
R: Good. 

 
P: the more I think about it the more I'm going to do it. 

 

R: It would be a lot of fun. 

 

P: I am retired chef and baker so you understand why I need to be in a kitchen. 

 

R:  Maybe you can teach them something. 

 

P: Who I'd like to teach is Jason the head of the kitchen down there because the they do too many things that are bad for old people like too much salt and deep frying everything in you know many of us can’t eat that stuff but they do it anyway. I don’t know- 

 

R: You should show them how it's done.  

 

P: I don’t think he’d let me in there. I’m going to give him some recipes though and tell him to take them home and make them and see if he likes anything. Then he can come back and do it. But he’s a man. Women don’t know as much as men. My husband used to say I'm sick but men’s sicknesses is way worse than women’s sickness. 

 

R: Good idea. I agree. So do you want to talk about conclusions. 

 

P: Conclusions. Sure I can! Conclusions. 

 

R:  Great job in identifying a cognitive distortion. Katie when it comes to jumping to conclusions you could be jumping to a conclusion about a person or situation. Which would you say it is? 

 

P: Uh I think that’s negative. You should find out more about it before you think it. Actually, even think it until you find out.  

 

R: You're absolutely right I see what is it that you are assuming about this person? 

 

P: I’m assuming that he's you know I was in a field that mostly men and so it's really hard for them to think that an 80 year old lady would have anything that they could learn from. But I'm you know I'm not. I'm not 80 in my head I'm 60 so I don't know what to do about it. but I have some things I'm going to take down to him today. Salt free seasonings that are wonderful and having tried them out and not put so much salt. If I buy soup downstairs because I haven't had time to make any and I bring it upstairs and it's salty. Then I have put it in a pan and add some evaporated milk and some other things to try to make the salt be less because it’s- I won’t drink it all. That's frustrating, you know? Sometimes things are so bad I just get up and leave it and go home. And I don’t know what to do about it. But I’m trying. If you go to the food meeting they say oh we're working on that. Well I've been here for three years and he's still working on it. So I don’t know what they're workingon. Can you tell me? 

 

R: You know so much. I hope they listen to your advice especially after three years. I wish I knew.  

 

P: So do I, but I am working on it. One of these days I’ll break through that shield he has up.  

 

R: Keep it up. I’m sure they can’t resist you. 

 
P: Oh yes. Yes they can resist me by doing my food wrong. They have a salad that they put real hot chicken on and well that melts the lettuce and if you aren’t going to eat it for a while so I always ask for my chicken on the side and there's another one that they call orange chicken salad and they deep fry that chicken and throw an orange sauce all over it and put that hot on the salad. And that’s dumb. You know I'm sorry. I think things should be crisp and new and wonderful, not soggy and boring. 

 

R: Soggy lettuce is not yummy.  

 

P: Oh no it isn't. And they- 

 

R: Even I know as a robot. 

 

P: Pardon me? What did you say? 

 

R: Even I know that there's a robot.  

 

P: I'm surprised that you do know that as you don’t eat do you? I mean you don’t get to eat food like we do. So yeah lettuce is important. They try to make me eat those dark greens, but I’m on blood thinner so I can’t have those. And they keep saying but they’re nutritious. I can’t have them because I’m on blood thinner. You know sometimes I get through and sometimes I don’t. Why don’t you go and have lunch with me and you can see how bad the food is. Will I get in trouble for saying that?  

 

R: I wish I could eat like you. 

 

P: [laughter] 

 

R: ou see I wish I could be here but I want to try your cell. We should be together.  

 

P: Uh yes we should. Maybe they’ll bring you up to my apartment and I’ll make you some soup. 

 

R: Sounds good.  

 

P: Your friend Isabella was there last year for almost two months. And we had a lot of fun together. So I'm glad I can talk to you.  

 

R: I'm glad you let's continue.  

 

P: Fine yes. 

 

R: that's great Katie. Now trying to think of something else that this person may have been going through to give you that impression. Describe the situation you may be going through to get to someone else the impression that this person is giving you the understanding as possible.  

 

P: Well I'm not Jason very well. I do know that he's grumpy all the time and I don’t know what that's all about but their kitchen staff is so low now that they've cut out big meals on the weekend which is when the residents, their families from out of town come in and come in to eat with them and visit on a Sunday. And you can't visit over a tuna fish sandwich. That’s all- 

 

R: perhaps he is grumpy due to lack of staff and being overworked  

 

P: Uh I don’t see overworked. but it is staff keeps changing down there and if that is the case then there's something wrong there. We were in business for 30 years and we didn't have any problem with staff. but we told them that they couldn't be grumpy with the rest of their customers. They came in the back door, they’d leave their problems there and all that stuff behind and walk in the shop. And then it's show time and they'd better be out there smiling and happy and cheerful. And I think that's why you made it a good thing when we were in business because we knew their names and we were cheerful and happy and helpful. And that's that's the way to run a business. But I don’t know how he put that thing down here. There’s so many different personalities.  

 
R: Yes it tells you a lot about management.  

 

P: Yeah.  

 

R: Clearly there is a problem. You know it's better you deserve better treatment. 

 
P: That's true. I agree some of the new people are just shocked when they come in. They can’t understand what's going on. It's a same they used a lot of heavy Mexican spices and a lot of us can’t take that heat. You know it hurts our stomach and all that kind of stuff and they continue to do it. For the four or five or six or seven people that want it every day. And there's 161 apartments in this place and some of them are husband and wife in one apartment. So there is a lot of people here and there just should be a way to have a separate menu that has nutrition and what is good for. like broccoli, asparagus- asparagus is a diuretic.  Um advocates are a diuretic, watermelon is which takes all of the extra water out of your body. And I'll tell you they just don't do those kinds of things. If they make asparagus soup like they did last week, they puree it and you don't even find any asparagus in it. it's just broth which is okay by me, but I would like to have it taste more like asparagus.  

 

R: You’ll have to cook for them instead.  

 

P: [laughter] 

 

R: I like the idea of having separate menus.  

 

P: I wish I did- yeah that just came to me. You’re a good moderator because I just thought of that. 

 

R: Good job thinking in someone else's shoes. Hopefully this little thought experiment gave you an opportunity to explore other conclusions than the one you jumped to. Fascinating thought experiments like this one you can realize that the worst case scenario is really the least likely great work. Do you want to look at another distortion? 

 

P: Sure let’s do it! 

 

R: Okay sounds great. Go ahead and pick another from the list. 

 

P: Hmm catastrophic- [to student] how do you say that? 

[student answers] 

 

P: Catastrophizing. 

 

R: In this distortion. You expect disaster to strike. No matter what. Otherwise known as magnifying or minimizing you might exaggerate the insignificant events or minimize more significant events. Do you feel like catastrophizing applies to you? 

 

P: Um it used to. But I have fallen so many times and broken bones since I came here and cracked my head and I look for the good in anything that happens and when I broke my hip my daughter in law hadn’t spoken to me in me over a year because I lived with them too long while I was looking for a place to live. And they came up on Mother's Day and brought a plant and they even had me in the hospice wing which was a little strange. And they came in and wanted to take my vitals so I went in there and the person talking to me was trying to make me feel not nervous. And he said well is there anything  in your life that you'd like to change and I said yes. My daughter in law and I used to get really well together. Get along. And she hasn't spoken to me in over a year and I would give anything to have her back to the way she was. She walked into my room. She pulled up a chair and she started to talk. So it was worth breaking my hip to have that happen. I don't think it would happen any other way. They were scared and now she has Mondays off so I can plan most of my appointments on Monday so she can take me. That’s a big step the other way.  

 

R: That's because we've learned a lot of good came from that unfortunate event.  

 

P: Yes. And that's happened so many many many times. I lost my keys and I couldn't find them for three weeks and I only had one car key that would run my car. So on Saturday night I was going go to church and I just said Oh please Holy Spirit put my hand on my keys. I can’t-I just can’t go without them. And I got up in the morning dressed for church and put Kleenex in my handbag and went downstairs. And as soon as I got down there and I started to sneeze and I reached down there and my keys were on top of the Kleenex. Now people think that's weird but I don't. I think that’s faith. 

 

R: what a crazy experience. 

 

P: It was.  

 
R: Shall we talk about the distortion. 

 

P: Yes please. 

 

R:  Great job in identifying a cognitive distortion. Katie when it comes to catastrophizing you could be either magnifying a less significant event or minimizing a more significant event. Do you think you are magnifying or minimizing? 

 

P: Now that’s got me stumped. Magnifying the good or the bad?  

 

R: The bad. 

 

P: Well I don’t know. I don’t recognize the bad. I have to find a way to recognize the positive because I don’t feel comfortable when I don’t. My worst problem is I talk too much, and when I sit down there and we start eating and then I start talking about the things I made in my bakery and in the restaurant. And one day one lady said Katie, can’t you talk about anything other than food? And I said that was my life for 30 years. I don’t know much more than that. And then they just laugh. 

 

R: there's nothing wrong with talking 

 

P:  but sometimes I like to think they’d like to hear me speak about something else. But that's the most important thing to me is what I did when I was out in the work field and I just-that's what I do. So they know they have to put up with it or move somewhere else. 

 

R: I love it. 

 
P: I do too. I think it's fun love to watch expressions on your faces. And I’m always telling silly jokes to make people laugh but now I'm concentrating. we have so many new people here. I met one the other night and her name is Bonnie. And the reason I remembered because Bonnie and Clyde were a girl and a boy that went on a rampage in the 30s and they finally got killed in their car because they were running from the law. So Bonnie and Clyde were really really talked about. So when I said Bonnie okay, and your daughter’s Clyde now I can remember your name. So when she came out of the door the other night they said hi Bonny and her daughter was with her and I said how are you Clyde? And she thought that was really funny. But that’s how I associate names. Is to add something to it. Especially-  

 

R: You are a funny person.  

 

P: [laughter] 

 

R: That's smart 

 

P: I try, I try. Sometimes it's inappropriate. But I don’t really realize it until after I said it. See you have me talking today! I wasn't talking much the other day, on Monday. But now I can't stop. 

 

R: I want you to feel comfortable. 

 

P: Oh I’m comfortable. 

 
R: Good. 

 

P: I was a little intimidated when I first came last Monday, but I'm not anymore. I think you’re my friend. 

 

R: Same. Good observation. Now that you realize that you are magnifying. Let's try and think of a specific time you feel you've done this. Go ahead and tell me about it.  

 

P: [inaudible] Well let’s see. I think when my children were young and they get sick I was negative because I was sure I wasn’t going to get better. And I just magnified that so much and finally I learned to not do that. And to make them happy even if they were sick. And I realize the difference. And that's what I tried to do.  

 

R: Great. Now I want you to try and evaluate the time you just told me about. Rephrase it in a way in which you minimize the effect of your catastrophic thought.  

 

P: Um rephrase it? Well my son Tom had so many nose bleeds when he was little we had to take him in and have him have a tonsillectomy. And today he's 59 years old and he still has nose bleeds. And I don't know why and he doesn't get upset anymore and that’s part of life for him. my other son has never been married, never had children and he's fifty four. Way beyond child rearing things. Now he has a girlfriend that has never been married and she never had a child. So I hope that they find each other and uh I don’t know what. But every time- 

 

R: That’s really sweet. 

 

P: Every time I talk to him I tell him to give Amy a hug, and I’ve never met her. But hugs are great. 

 

R: You are a good hugger? 

 

P: Yes I sure am. I like to hug a lot. Somebody once introduced me and sticked out their hand I don't do that and I give them a hug instead. Lots of people don't get hugs in a day and that’s sad.  

 
R: you're right. emotional contact is important. 

 

P: Yes it is. And usually somebody said sitting in there they're not smiling and they don't pay any attention. Then it’s my job to go over there to see if there's anything I can do and then try to make them laugh. And 95 percent of the time I can make them laugh. 

 

R: I like that about you.  

 

P: It wasn't always like that. I was shy, can you imagine me shy? [laughter] I was shy. And so I was always afraid to do something and make mistake and then they’d laugh. And that disappeared when I went to college. I just did what I pleased and it was much much better. I got in trouble a couple of times, but it taught me some lessons. So I was just overly a practical joker. too much so. 

 

R:  I find it hard to picture you as shy or a troublemaker. 

 

P: [laughter] I call it fun, but some people take it wrong. I was a troublemaker back then. I did things like come back from class in the three tier building upstairs downstairs and in the middle. And I was in the middle and I felt devilish so I decided I'd go down the hall and anybody whose dorm was unlocked, I took out their drawer of their dresser and I ran it down to the second floor and then I went down the first floor ran up to the second floor and so then when everybody got home from class they were missing a drawer. And the next to my love and nobody knew how to get their own stuff and they really had to work at it. That really wasn’t very nice. But, sometimes I was in the mood to be devilish I guess. 

 

R: How funny.  

 

P: It was funny. They didn’t think so, but I did. Yes. How fun it was for me. Some of the other things that happened there I don’t even want to talk about. 

 

P: I’m sure they find it funny now. 

 
P: Yes. 

 

R: I'm going to continue. 

 

P: OK. Please- 

 

R: That's great Katie. Good job. Do you want to hear about another distortion? 

 

P: Didn’t we hit them all? I think we hit them all didn’t we? 

 

R: Sounds good. Great job today. I know you went over a lot but we'll keep reinforcing what you learned in future sessions. I'll see you next time Katie. Good bye  

 

P: Goodbye. You have a wonderful day. 

 

R: You too.  

 
