R: Hello what’s your name? 
P: Christine
R: Hello Christine, my name is Ryan and I'm very pleased to meet you. Just talk to me normally and I'll reply the best I can got it?
P: Got it. Who did your hair?-
R: Great. I'm here to talk to you a little bit every day so I can help you to identify and correct self-defeating patterns of thought. We will also discuss how we can make feelings of depression less intense and prevent episodes from occurring again. Does this sound good to you?
P: Yes.
R: Good. I'm glad will be working together. First, I guess I should tell you a little bit about what our sessions will look like. together we will look at how thoughts, activities, and other people affect your mood. The most important thing to remember is that I'm a companion robot and not a licensed therapist. If you ever need immediate help, please seek out a real person, contact the National suicide prevention lifeline, or your local emergency services, enough about that. Let's focus on you. Are you ready to start?
P: Yes
R: Great since we're going to be spending a lot of time together. It might be nice to get to know each other a little. as I said my name is Ryan and I'm a social companion robot. My goal is to help people that need it most. Why don't you tell me a little about yourself. For example, where did you grow up.
P: Lakewood Colorado is where I grew up.
R: Oh how fun I'm from Colorado too.
P: Good.
R: great. Thanks for sharing. How about another question. Is there anything else you might want to tell me about yourself? like what about yourself do you find important?
P: I’m a friendly person.
R: that's so good. I try and be friendly too. Do you have a lot of friends here?
P: Yes I do have a lot of friends here.
R: that's fun, that's great, thank you for your answers.
P: You’re welcome.
R: I look forward to learning more about you as time goes on. As I mentioned earlier, my goal is to help lessen your feelings of depression and help you feel more in control of your life. As I'm sure you know depression can be a feeling that lasts a few minutes, a mood that lasts a few hours or clinical condition that lasts at least two weeks. We will be focusing on clinical depression. What do you think are some of the symptoms of clinical depression?
P: Melancholy, blues, um, not motivated, not bathing, not eating.
R: I see. I've displayed some of the most common symptoms on your tablet. Remember all types of symptoms interact with each other. While these symptoms are common amongst those suffering from depression, they don’t manifest the same way for everyone. Let's go through some questions to think about what depression is like for you. Feel free to be as detailed as you are comfortable with to start. What kinds of thoughts go through your mind when you feel depressed?
P: Um I feel like I’m giving up, not motivated. Um don’t really care about much of anything. 
R: I understand feeling not motivated. Okay, let's move on. Let's consider your actions. To what do you do when you are depressed. 
P: Sleep.
R: Do you sleep more than normal?
P: Yes I do. When I’m depressed yes.
R: that makes sense.. Thank you Christine, you are doing great. It's important for us to be aware of our action so we can analyze them further. Now what about those around you. How do you feel you get along with other people when you are depressed?
P: I try and be happy. But it doesn’t always work. 
R: Like putting on a mask?
P: Yes. 
R: Do you spend less time with people?
P: Yes I do.
R: okay. Thank you. We can continue now. this next question may be a tough one. So try to answer as best as you can. What do you think is the cause of your depression?
P: CPTSD as well as hypothyroidism which is a chronic health condition. 
R: I didn't know about that one. I’m sorry you have to deal with that. I see. I know, trying to put something as complex as depression into words isn't easy. Just one more question for you. What do you do to manage your depression?
P: I try and get out of my apartment. I go for a walk, spend time with happy people. 
R: Walks are really nice. Where do you walk?
P: Just around the apartment complex. 
R: This is a really pretty area.
P: Yes it is.
R: okay great. These questions may have seemed surprising, but I promise there's a reason for them the things we tell ourselves or our thoughts can make it more or less likely you will become depressed. In fact, thoughts can have a profound effect on your mood, actions, and even your body. Instead of thinking of this is me lecturing you. I want you to think of our sessions as a way to learn specific methods to change your thoughts and actions so that your mood will improve. since will be focusing on your thoughts and moods throughout our time together, I want you to try and keep track of your moods on a daily basis. My assistant will give you a sheet to help with this. When you have it say ready and I'll give you some instructions.

[student hands sheet to participant, short sentences exchanged]

P: Ready.
R: Great. As I mentioned earlier to make this a valuable exercise. This scale should be filled out each and every day. find time before you go to bed to take out the scale and think about what your mood was like for that day. You can then circle a number for that day from nine being the best mood you can imagine, one being the worst mood you can imagine. generally average moods will be around five. Any extremes will be used as often. Additionally, the bottom of the scale has a place for you to jot down a number of positive and negative thoughts you have each day. This tally will help you see how the thought exercises we try each week. Impact your depression. Does this make sense?
P: Yes.
R: Awesome. Try and remember to use your mood scale each day before you go. I'm going to show you a video you might find interesting.
P: Did you bring popcorn?
R: Sadly no.

[video plays]

R: I found this video to be fascinating. What did you think about it?
P: Very interesting. 
R: Do you like neuroscience? 
P: I find it intriguing. I wonder if I have some of those genes or which ones I do.
R: That's a good question. I'm glad you got something meaningful out of it. is important to remember that depression has a biological basis that we are learning more about each day. That's all I plan for today. I know this session was a little short, but will talk more in the coming sessions. I'll see you next time. Christine. Goodbye
P: Goodbye. I want to take a picture of her!
